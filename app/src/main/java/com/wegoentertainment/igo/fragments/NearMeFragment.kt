package com.wegoentertainment.igo.fragments


import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.wegoentertainment.igo.BuildConfig
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.adapters.NearEventAdapter
import com.wegoentertainment.igo.db.CloseEventWithPhotos
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.PreferenceHelper
import com.wegoentertainment.igo.utils.showToast
import com.wegoentertainment.igo.viewmodels.NearEventViewModel

class NearMeFragment : Fragment() {

    private val TAG = "NearMeFragment"
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var recyclerView : RecyclerView? = null
    private var adapter: NearEventAdapter = NearEventAdapter()
    private var prefs: SharedPreferences? = null
    private lateinit var eventViewModel: NearEventViewModel
    private var latlong: String? = null
    private var refresher: SwipeRefreshLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        if (checkPermissions()) {
            getLocation()
        } else {
            requestPermissions()
        }


    }

    @SuppressLint("MissingPermission")
    fun getLocation(){
        mFusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                location?.let { it -> eventViewModel.updateLocation(it) }
                Log.d("Location","${location?.latitude} ${location?.longitude}")
                // Got last known location. In some rare situations this can be null.
            }.addOnFailureListener {
                Toast.makeText(activity, "No location detected", Toast.LENGTH_LONG).show()
            }
    }


    private fun initializeUI(){
        activity?.run{
            eventViewModel = ViewModelProviders.of(this).get(NearEventViewModel::class.java)
            eventViewModel.currentLocation?.observe(this, Observer {
                latlong = it.latitude.toString() + "," + it.longitude
                eventViewModel.fetchData(prefs!!.getString(Constants.token, Constants.token)!!,latlong!!)
            })
            eventViewModel.getLoader().observe(this, Observer<Boolean> { loading ->
                refresher?.isRefreshing = loading
            })
            eventViewModel.getErrors().observe(this, Observer<String> { errorMsg ->
                setError(errorMsg)
            })
            eventViewModel.getEventData().observe(this, Observer<List<CloseEventWithPhotos>> {
                updateData(it)
            })

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_near_me, container, false)

        prefs = PreferenceHelper.defaultPrefs(activity?.applicationContext!!)

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView?.layoutManager = LinearLayoutManager(context)
        recyclerView?.adapter = adapter
        refresher?.setOnRefreshListener {
            refresher?.isRefreshing = true
            eventViewModel.fetchData(prefs!!.getString(Constants.token, Constants.token)!!,latlong!!)
        }

        initializeUI()
        return view
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            activity!!,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            showSnackbar(R.string.location_needed,
                View.OnClickListener {
                    startLocationPermissionRequest()
                })

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            context!!,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }


    private fun showSnackbar(mainTextStringId: Int, listener: View.OnClickListener) {

        Toast.makeText(activity, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                grantResults.isEmpty() -> // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i(TAG, "User interaction was cancelled.")
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                    // Permission granted.
                    getLocation()
                }
                else -> // Permission denied.
                    showSnackbar(R.string.permission_denied_explanation,
                        View.OnClickListener {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                BuildConfig.APPLICATION_ID, null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        })
            }
        }
    }

    // ******** End Permissions *********/

    private fun updateData(data: List<CloseEventWithPhotos>){
        adapter.mEvents.clear()
        adapter.mEvents.addAll(data)
        Log.d("nearevent",data.size.toString())
        adapter.notifyDataSetChanged()
    }

    private fun setError(msg:String){
        context?.showToast(msg)
    }


}

