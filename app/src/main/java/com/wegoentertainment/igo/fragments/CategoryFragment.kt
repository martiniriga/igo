package com.wegoentertainment.igo.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.adapters.CategoryAdapter
import com.wegoentertainment.igo.models.Category
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.PreferenceHelper
import com.wegoentertainment.igo.utils.showToast
import com.wegoentertainment.igo.viewmodels.CategoryViewModel

class CategoryFragment : Fragment() {


    private var recyclerView : RecyclerView? = null
    private var adapter: CategoryAdapter = CategoryAdapter()
    private var prefs: SharedPreferences? = null
    private var refresher: SwipeRefreshLayout? = null
    private lateinit var categoryViewModel: CategoryViewModel


    private fun initializeUI(){
        activity?.run{
            categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
            categoryViewModel.getCategoryData().observe(this, Observer<List<Category>> { categoryList ->
                updateData(categoryList)
            })
            categoryViewModel.getErrors().observe(this, Observer<String> { errorMsg ->
                setError(errorMsg)
            })
            categoryViewModel.getLoader().observe(this, Observer<Boolean> { loading ->
                refresher?.isRefreshing = loading
            })

            categoryViewModel.fetchData(prefs!!.getString(Constants.token, Constants.token)!!)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_category, container, false)
        prefs = PreferenceHelper.defaultPrefs(activity?.applicationContext!!)

        recyclerView = view.findViewById(R.id.recyclerView)
        refresher = view.findViewById(R.id.refresher)


        recyclerView?.layoutManager = LinearLayoutManager(context)
        recyclerView?.adapter = adapter

        refresher?.setOnRefreshListener {
            refresher?.isRefreshing = true
            categoryViewModel.fetchData(prefs!!.getString(Constants.token, Constants.token)!!)
        }

        initializeUI()

        return view
    }

    private fun updateData(data: List<Category>){
        adapter.mCategories.clear()
        adapter.mCategories.addAll(data)
        adapter.notifyDataSetChanged()
    }

    private fun setError(msg:String){
        context?.showToast(msg)
    }

}