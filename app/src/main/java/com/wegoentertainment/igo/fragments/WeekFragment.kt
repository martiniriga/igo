package com.wegoentertainment.igo.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.adapters.EventAdapter
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.PreferenceHelper
import com.wegoentertainment.igo.utils.showToast
import com.wegoentertainment.igo.viewmodels.EventViewModel


class WeekFragment : Fragment() {


    private var recyclerView : RecyclerView? = null
    private var adapter: EventAdapter = EventAdapter()
    private var prefs: SharedPreferences? = null
    private var refresher: SwipeRefreshLayout? = null
    private lateinit var eventViewModel: EventViewModel


    private fun initializeUI(){
        eventViewModel = ViewModelProviders.of(this).get(EventViewModel::class.java)
        eventViewModel.getEventData().observe(this, Observer<List<EventWithPhotos>> { eventList ->
            updateData(eventList)
        })
        eventViewModel.getLoader().observe(this, Observer<Boolean> { loading ->
            refresher?.isRefreshing = loading
        })
        eventViewModel.getErrors().observe(this, Observer<String> { errorMsg ->
            setError(errorMsg)
        })

        eventViewModel.fetchData(prefs!!.getString(Constants.token,Constants.token)!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_week, container, false)
        prefs = PreferenceHelper.defaultPrefs(activity?.applicationContext!!)

        recyclerView = view.findViewById(R.id.recyclerView)
        refresher = view.findViewById(R.id.refresher)


        recyclerView?.layoutManager = LinearLayoutManager(context)
        recyclerView?.adapter = adapter

        refresher?.setOnRefreshListener {
            eventViewModel.fetchData(prefs!!.getString(Constants.token,Constants.token)!!)
        }

        initializeUI()

        return view
    }

    private fun updateData(data: List<EventWithPhotos>){
        adapter.mEvents.clear()
        adapter.mEvents.addAll(data)
        adapter.notifyDataSetChanged()
    }

    private fun setError(msg:String){
        context?.showToast(msg)
    }
}
