package com.wegoentertainment.igo.holders

import android.content.Intent
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.activities.CategoryActivity
import com.wegoentertainment.igo.models.Category

class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    
    private val mTitleTextView: TextView = itemView.findViewById(R.id.txttitle)

    fun bindData(category: Category) {
        mTitleTextView.text = category.name
        itemView.setOnClickListener{
            val intent = Intent(itemView.context, CategoryActivity::class.java)
            intent.putExtra("category", category.name)
            itemView.context.startActivity(intent)
        }
    }

}

