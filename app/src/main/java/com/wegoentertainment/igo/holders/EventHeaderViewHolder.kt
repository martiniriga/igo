package com.wegoentertainment.igo.holders


import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.activities.EventDetailActivity
import com.wegoentertainment.igo.db.EventWithPhotos
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class EventHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val headerDay: TextView = itemView.findViewById(R.id.day)
    private val headerDate: TextView = itemView.findViewById(R.id.date)
    private val mTitleTextView: TextView = itemView.findViewById(R.id.txttitle)
    private val mPhotoImageView: ImageView = itemView.findViewById(R.id.iv_photo)


    fun bindData(obj: EventWithPhotos) {
        val event = obj.event
        obj.photos?.let{
            if(obj.photos!!.isNotEmpty()){
                Glide.with(itemView.context)
                    .load(obj.photos?.get(0)?.photo)
                    .into(mPhotoImageView)
            }
        }
        var currentItemDate = Date()
        val dformat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            currentItemDate = dformat.parse(event?.start_time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val day = SimpleDateFormat("EEE", Locale.getDefault()).format(currentItemDate)
        val date = SimpleDateFormat("dd MMMM", Locale.getDefault()).format(currentItemDate)
        headerDay.text = day
        headerDate.text = date
        mTitleTextView.text = event?.caption
        itemView.setOnClickListener{
            val intent = Intent(itemView.context, EventDetailActivity::class.java)
            intent.putExtra("source", "event")
            intent.putExtra("eventId", event?.id)
            itemView.context.startActivity(intent)
        }
    }
}