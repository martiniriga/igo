package com.wegoentertainment.igo.holders

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.activities.EventDetailActivity
import com.wegoentertainment.igo.db.CloseEventWithPhotos
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class NearEventHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mTitleTextView: TextView = itemView.findViewById(R.id.txttitle)
    private val mVenueTextView: TextView = itemView.findViewById(R.id.txtvenue)
    private val mTimeTextView: TextView = itemView.findViewById(R.id.txttime)
    private val mPhotoImageView: ImageView = itemView.findViewById(R.id.imgbrand)


    fun bindData(obj: CloseEventWithPhotos) {
        val bookmark = obj.event
        obj.photos?.let{
            if (obj.photos!!.isNotEmpty()) {
                Glide.with(itemView.context)
                    .load(obj.photos!![0].photo)
                    .into(mPhotoImageView)
            }
        }



        var startItemDate = Date()
        var endItemDate = Date()
        val dformat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            startItemDate = dformat.parse(bookmark?.start_time)
            endItemDate = dformat.parse(bookmark?.end_time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timed = SimpleDateFormat("hh:mm a", Locale.getDefault()).format(startItemDate) + " - " + SimpleDateFormat(
            "hh:mm a",
            Locale.getDefault()
        ).format(endItemDate)

        val shortdate = timed + " " + SimpleDateFormat("dd, M, yy", Locale.getDefault()).format(startItemDate)

        mTitleTextView.text = bookmark?.title
        mVenueTextView.text = bookmark?.host
        mTimeTextView.text = shortdate


        itemView.setOnClickListener {
            val intent = Intent(itemView.context, EventDetailActivity::class.java)
            intent.putExtra("source", "bookmark")
            intent.putExtra("eventId", bookmark?.id)
            itemView.context.startActivity(intent)
        }


    }


}

