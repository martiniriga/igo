package com.wegoentertainment.igo.holders

import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.models.Category

class InterestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    
    private val btnInterest: Button = itemView.findViewById(R.id.btnInterest)

    fun bindData(category: Category,clickListener: (Category) -> Unit) {
        btnInterest.text = category.name
        btnInterest.setOnClickListener{
//            it.visibility = View.INVISIBLE
            clickListener(category)
        }
    }

}

