package com.wegoentertainment.igo.holders

import android.content.Intent
import android.view.ContextMenu
import android.view.Menu
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.activities.EventDetailActivity
import com.wegoentertainment.igo.db.BookmarkWithPhotos
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BookmarkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    
    private val mTitleTextView: TextView = itemView.findViewById(R.id.txttitle)
    private val mLocationTextView: TextView = itemView.findViewById(R.id.txtvenue)
    private val mDateTextView: TextView = itemView.findViewById(R.id.txtdate)
    private val mDayTextView: TextView = itemView.findViewById(R.id.txtday)
    private val mVenueTextView: TextView = itemView.findViewById(R.id.txtvenue)
    private val mTimeTextView: TextView = itemView.findViewById(R.id.txttime)


    fun bindData(obj: BookmarkWithPhotos) {
        val bookmark = obj.event
        var startItemDate = Date()
        var endItemDate = Date()
        val dformat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            startItemDate = dformat.parse(bookmark?.start_time)
            endItemDate = dformat.parse(bookmark?.end_time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timed = SimpleDateFormat("hh:mm a", Locale.getDefault()).format(startItemDate) + " - " + SimpleDateFormat(
            "hh:mm a",
            Locale.getDefault()
        ).format(endItemDate)
        val day = SimpleDateFormat("EEEE", Locale.getDefault()).format(startItemDate)
        val shortdate = SimpleDateFormat("dd, M, yy", Locale.getDefault()).format(startItemDate)

        mTitleTextView.text = bookmark?.title
        mLocationTextView.text = bookmark?.venue
        mDateTextView.text = shortdate
        mDayTextView.text = day
        mVenueTextView.text = bookmark?.venue
        mTimeTextView.text = timed


        itemView.setOnClickListener{
            val intent = Intent(itemView.context, EventDetailActivity::class.java)
            intent.putExtra("source", "bookmark")
            intent.putExtra("eventId", bookmark?.id)
            itemView.context.startActivity(intent)
        }

        itemView.setOnCreateContextMenuListener{ contextMenu: ContextMenu, _: View, _: ContextMenu.ContextMenuInfo ->
                //menuInfo is null
            contextMenu.add(Menu.NONE, R.id.delete_collection, Menu.NONE, R.string.deletebookmark)
        }

    }



}

