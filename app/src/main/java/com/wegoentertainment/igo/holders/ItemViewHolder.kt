package com.wegoentertainment.igo.holders

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.activities.EventDetailActivity
import com.wegoentertainment.igo.db.EventWithPhotos

class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

    private val mPhotoImageView: ImageView = itemView.findViewById(R.id.iv_photo)
    private val mTitleTextView: TextView = itemView.findViewById(R.id.txttitle)


    fun bindData(obj: EventWithPhotos) {
        val event = obj.event
        Glide.with(itemView.context)
            .load(obj.photos?.get(0)?.photo)
            .into(mPhotoImageView)
        mTitleTextView.text = event?.caption
        itemView.setOnClickListener{
            val intent = Intent(itemView.context, EventDetailActivity::class.java)
            intent.putExtra("source", "event")
            intent.putExtra("eventId", event?.id)
            itemView.context.startActivity(intent)
        }
    }

}

