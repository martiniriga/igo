package com.wegoentertainment.igo.models.gson.bookmarkData

import com.wegoentertainment.igo.models.Bookmark

data class BookmarkData(
    val events: List<Bookmark>
)