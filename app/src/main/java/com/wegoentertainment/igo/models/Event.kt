package com.wegoentertainment.igo.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.wegoentertainment.igo.db.DateConverter

@Entity(tableName = "events")
data class Event(
    var caption: String?,
    var category: String,
    var contact_email: String?,
    var contact_phone: String?,
    var description: String,
    var end_time: String,
    var host: String,
    @PrimaryKey
    var id: Int,
    @Ignore
    var photos: List<Photo>,
    var price: Int,
    @Ignore
    var pricing_types: List<Any>?,
    var share_url: String?,
    @TypeConverters(DateConverter::class)
    var start_time: String,
    var title: String,
    var venue: String,
    var venue_coordinates: String
)
{
    constructor() : this("","","","",
        "","","",0,ArrayList<Photo>(),0,null,
        "","","","","")
}