package com.wegoentertainment.igo.models.gson.addBookmarkData

data class AddBookmarkQuery(
    val data: String,
    val status: String
)
