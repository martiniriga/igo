package com.wegoentertainment.igo.models.gson.bookmarkData

data class BookmarksResponse(
    val `data`: BookmarkData
)