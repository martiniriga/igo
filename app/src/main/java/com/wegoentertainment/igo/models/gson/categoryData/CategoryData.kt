package com.wegoentertainment.igo.models.gson.categoryData

import com.wegoentertainment.igo.models.Category

data class CategoryData(
    val categories: List<Category>
)