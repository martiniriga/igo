package com.wegoentertainment.igo.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "bookmarkphotos")
data class BookmarkPhoto(
    @PrimaryKey
    val id: Int,
    val description: String,
    val photo: String,
    var eventId:Int
)