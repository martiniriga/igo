package com.wegoentertainment.igo.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.wegoentertainment.igo.db.DateConverter

@Entity(tableName = "closeevents")
data class CloseEvent(
    @PrimaryKey
    var id: Int,
    var title: String,
    var host: String,
    var price: Int,
    @TypeConverters(DateConverter::class)
    var start_time: String,
    @TypeConverters(DateConverter::class)
    var end_time: String,
    var contact_email: String?,
    var contact_phone: String?,
    var caption: String?,
    var venue_coordinates: String,
    var share_url: String?,
    var distance: Float,
    @Ignore
    var photos: List<Photo>
) {
    constructor() : this(
        0, "", "", 0,
        "", "","", "", "", "", "", 0f,ArrayList<Photo>())
}