package com.wegoentertainment.igo.models.gson.Login

data class LoginQuery(
    val token: String,
    val status: String
)
