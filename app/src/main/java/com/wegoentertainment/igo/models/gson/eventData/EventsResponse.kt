package com.wegoentertainment.igo.models.gson.eventData

data class EventsResponse(
    val `data`: EventData
)