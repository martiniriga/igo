package com.wegoentertainment.igo.models

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "interests")
data class Interest(
    @PrimaryKey
    val id: Int,
    @NonNull
    val name: String
)