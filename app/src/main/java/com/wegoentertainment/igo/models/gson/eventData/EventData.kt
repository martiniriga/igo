package com.wegoentertainment.igo.models.gson.eventData

import com.wegoentertainment.igo.models.Event

data class EventData(
    val events: List<Event>
)