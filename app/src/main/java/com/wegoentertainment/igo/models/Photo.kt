package com.wegoentertainment.igo.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "photos")
//    ,foreignKeys = [
//    ForeignKey(entity = Event::class,
//        parentColumns = ["id"],
//        childColumns = ["event_id"])
//])
data class Photo(
    @PrimaryKey
    val id: Int,
    val description: String,
    val photo: String,
    var eventId:Int
)