package com.wegoentertainment.igo.models.gson.eventData

data class EventsCloseResponse(
    val `data`: EventCloseData
)