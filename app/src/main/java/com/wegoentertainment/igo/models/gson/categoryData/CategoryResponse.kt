package com.wegoentertainment.igo.models.gson.categoryData

data class CategoryResponse(
    val `data`: CategoryData
)