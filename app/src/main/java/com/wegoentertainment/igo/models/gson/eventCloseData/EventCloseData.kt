package com.wegoentertainment.igo.models.gson.eventData

import com.wegoentertainment.igo.models.CloseEvent

data class EventCloseData(
    val events: List<CloseEvent>
)