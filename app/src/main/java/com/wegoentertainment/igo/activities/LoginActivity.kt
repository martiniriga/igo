package com.wegoentertainment.igo.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.PreferenceHelper.get
import com.wegoentertainment.igo.utils.PreferenceHelper.set
import com.wegoentertainment.igo.utils.showToast
import com.wegoentertainment.igo.viewmodels.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient
    private var loginViewModel: LoginViewModel = LoginViewModel()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        setContentView(R.layout.activity_login)


        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        loginViewModel.getTokenFromServer().observe(this, Observer { token ->
            hideProgressDialog()
            token?.let{
                saveToken(token)
            }
            goHome()
        })
        loginViewModel.getErrorUpdates().observe(this,Observer { data ->
            hideProgressDialog()
            data?.let {
                showToast(data, Toast.LENGTH_LONG)
            }
        })

        btnlogin.setOnClickListener {
            showProgressDialog()
            val loggedIn = googleSignUp()
            if(!loggedIn){
                val signInIntent = googleSignInClient.signInIntent
                startActivityForResult(signInIntent, RC_SIGN_IN)
            }
            hideProgressDialog()
        }

//        checkPlayServices()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.server_client_id))
            .requestScopes(Scope("profile"))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
        auth = FirebaseAuth.getInstance()

        btnskip.setOnClickListener {
            goHome()
        }
        btnStart.setOnClickListener { toggleBtns() }
    }

    private fun toggleBtns() {
        btnskip.visibility = View.VISIBLE
        btnStart.visibility = View.GONE
        btnlogin.visibility = View.VISIBLE
    }

    private fun goHome(){
        val intent = Intent(this,ExploreActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun saveToken(token:String) {
        prefs[Constants.token] = token
        Log.d("server token",token)
    }


    override fun onStart() {
        super.onStart()
        val tokenStr: String? = prefs[Constants.token]

        val currentUser = auth.currentUser

        Log.d("currentUser",currentUser.toString())

        tokenStr?.let{
            Log.d("server token",tokenStr)
            goHome()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)

                firebaseAuthWithGoogle(account!!)
                loginViewModel.handleSignInResult(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("Login", "Google sign in failed", e)
                // ...
            }

        }
    }

    // If am to log users on firebase
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d("Login", "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Login", "signInWithCredential:success")
                    val user = auth.currentUser
                    Log.d("Login",user.toString())
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Login", "signInWithCredential:failure", task.exception)
                }
            }
    }

    private fun googleSignUp():Boolean {
        try {
            val account = GoogleSignIn.getLastSignedInAccount(applicationContext)
            account?.idToken?.let{
                loginViewModel.sendGoogleToken(account.idToken!!)
                return true
            }
        } catch (e: Exception) {
            Log.w("LoginActivity", "signInResult:failed code=" + e.message)
            showToast("Google sign in failed",Toast.LENGTH_LONG)
        }
        return false
    }


    fun checkPlayServices(): Boolean {
        val googleAPI = GoogleApiAvailability.getInstance()
        val result = googleAPI.isGooglePlayServicesAvailable(this)
        if (result != ConnectionResult.SUCCESS) {
            //Any random request code
            val playRequestCode = 1000
            //Google Play Services app is not available or version is not up to date. Error the
            // error condition here
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(
                    this, result,
                    playRequestCode
                ).show()
            }
            return false
        }
        //Google Play Services is available. Return true.
        return true
    }
//    private fun sendToken(providertoken: String) {
//        RestUrl.setSectionURI(RestEndPoint.URL_SIGN_IN_GOOGLE)
//        val myDialog = RestEndPoint.showProgressDialog(this@LoginActivity, getString(R.string.signing_in))
//        val params = HashMap<String, String>()
//
//        params[PrefManager.google_token] = providertoken
//        Log.d("google", providertoken)
//
//        val stringRequest = object : JsonObjectRequest(RestUrl.getEndPoint(), JSONObject(params),
//            object : Response.Listener<JSONObject>() {
//                fun onResponse(response: JSONObject) {
//                    myDialog.dismiss()
//                    try {
//                        pref.setToken(response.getString(RestEndPoint.token))
//                    } catch (e: JSONException) {
//                        e.printStackTrace()
//                    }
//
//                    if (pref.hasDevice_id()) {
//                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
//                        startActivity(intent)
//                        finish()
//                    } else {
//                        getDeviceInfo()
//                    }
//                }
//            },
//            object : Response.ErrorListener() {
//                fun onErrorResponse(error: VolleyError) {
//                    myDialog.dismiss()
//                    val response = error.networkResponse
//                    if (response != null) {
//                        try {
//                            val res =
//                                String(response!!.data, HttpHeaderParser.parseCharset(response!!.headers, "utf-8"))
//                            Log.d("error", res)
//                            val obj = JSONObject(res)
//                            val message = obj.getString("message")
//                            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
//                        } catch (e1: UnsupportedEncodingException) {
//                            e1.printStackTrace()
//                        } catch (e2: JSONException) {
//                            e2.printStackTrace()
//                        }
//
//                    }
//                }
//            }) {
//
//        }
//        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
//    }

//     fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == RC_SIGN_IN) {
//            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
//            loginViewModel.handleSignInResult(task)
//        }
//    }

//    fun onConnectionFailed(connectionResult: ConnectionResult) {
//        if (!connectionResult.hasResolution()) {
//            apiAvailability!!.getErrorDialog(this@LoginActivity, connectionResult.getErrorCode(), requestcode).show()
//        }
//    }

//    private fun getDeviceInfo() {
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                android.Manifest.permission.READ_PHONE_STATE
//            ) !== PackageManager.PERMISSION_GRANTED
//        ) {
//            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_PHONE_STATE), REQUEST_READ)
//        } else {
//            val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//            deviceid = telephonyManager.deviceId
//            if (deviceid != null) {
//                sendDeviceDetails()
//            }
//        }
//    }
//
//    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        if (requestCode == REQUEST_READ) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                getDeviceInfo()
//            } else {
//                Toast.makeText(this@LoginActivity, getString(R.string.enable_device), Toast.LENGTH_SHORT).show()
//            }
//        }
//    }
//
//    private fun sendDeviceDetails() {
//        RestUrl.setSectionURI(RestEndPoint.URL_ADD_DEVICE)
//        val params = HashMap<String, String>()
//        val devicename = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL
//        params[PrefManager.device_id] = deviceid!!
//        params[PrefManager.device_name] = devicename
//        if (pref.hasToken())
//            params["token"] = pref.getToken()
//        val stringRequest = object : JsonObjectRequest(RestUrl.getEndPoint(), JSONObject(params),
//            object : Response.Listener<JSONObject>() {
//                fun onResponse(response: JSONObject) {
//                    pref.setDevice_id(deviceid)
//                    pref.setDevice_name(devicename)
//                    if (!pref.hasfcm_saved())
//                        sendRegistrationToServer()
//                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
//                    startActivity(intent)
//                    finish()
//                }
//            },
//            object : Response.ErrorListener() {
//                fun onErrorResponse(error: VolleyError) {
//                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
//                    startActivity(intent)
//                    finish()
//                }
//            }) {
//
//        }
//        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
//    }
//
//    private fun sendRegistrationToServer() {
//        val RestUrl = RestEndPoint()
//        RestUrl.setSectionURI(RestEndPoint.URL_ADD_FCM)
//        val params = HashMap<String, String>()
//        params["id"] = pref.getFcm_token()
//        if (pref.hasToken())
//            params["token"] = pref.getToken()
//        val jsonObjectRequest = object : JsonObjectRequest(RestUrl.getEndPoint(), JSONObject(params),
//            object : Response.Listener<JSONObject>() {
//                fun onResponse(response: JSONObject) {
//                    Log.d("Noma", "fcm token with user sent" + pref.getFcm_token())
//                    pref.setfcm_saved()
//                }
//            },
//            object : Response.ErrorListener() {
//                fun onErrorResponse(error: VolleyError) {}
//            }) {
//
//        }
//        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest)
//    }



    companion object {
        private val RC_SIGN_IN = 9001
    }

}
