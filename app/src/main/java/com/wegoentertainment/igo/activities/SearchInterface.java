package com.wegoentertainment.igo.activities;

/**
 * Created by marti on 08-May-18.
 */

public interface SearchInterface
{
    void filter(String searchtext) ;
}
