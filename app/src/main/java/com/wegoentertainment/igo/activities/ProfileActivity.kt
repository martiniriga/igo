package com.wegoentertainment.igo.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.PreferenceHelper.get
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity() {
    private var tokenString:String? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_explore -> {
                //already here
                val intent3 = Intent(this, ExploreActivity::class.java)
                startActivity(intent3)
                finish()
            }

            R.id.navigation_bookmarks -> {
                val intent2 = Intent(this, BookmarksActivity::class.java)
                startActivity(intent2)
                finish()
            }

            R.id.navigation_profile -> {
//                We are here
            }
        }
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        tokenString = prefs[Constants.token]
        setUpUI()
    }

    private fun setUpUI() {
        privacy.setOnClickListener {
            val uri = Uri.parse("https://igoapp.fun/privacy")
            val webIntent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(webIntent)
        }
        tokenString?.let{
            login.text = getString(R.string.logout)
            login.setOnClickListener{
                logOut()
            }
        }
        if(tokenString == null){
            login.setOnClickListener{
                goToLogin()
            }
        }

        update_interests.setOnClickListener {
            goToInterests()
        }

    }

    private fun goToInterests() {
        val intent = Intent(this,InterestsActivity::class.java)
        intent.putExtra("source","profile")
        startActivity(intent)
    }

    private fun goToLogin() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun logOut() {
        goToLogin()
    }
}
