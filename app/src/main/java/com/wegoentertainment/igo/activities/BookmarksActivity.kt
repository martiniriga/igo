package com.wegoentertainment.igo.activities


import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.adapters.BookmarkAdapter
import com.wegoentertainment.igo.db.BookmarkWithPhotos
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.PreferenceHelper.get
import com.wegoentertainment.igo.utils.showToast
import com.wegoentertainment.igo.viewmodels.BookmarksViewModel
import kotlinx.android.synthetic.main.activity_bookmarks.*


class BookmarksActivity : BaseActivity() {

    private lateinit var bookmarksViewModel: BookmarksViewModel
    private var adapter: BookmarkAdapter = BookmarkAdapter()
    private var tokenstr:String? = null
    private var eventlist:List<BookmarkWithPhotos> = ArrayList()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_explore -> {
                val intent2 = Intent(this, ExploreActivity::class.java)
                startActivity(intent2)
                finish()
            }

            R.id.navigation_bookmarks -> {
                //already here
            }

            R.id.navigation_profile -> {
                val intent3 = Intent(this, ProfileActivity::class.java)
                startActivity(intent3)
                finish()
            }
        }
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookmarks)
//        setSupportActionBar(toolbar)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        recyclerView?.layoutManager = LinearLayoutManager(applicationContext)
        recyclerView?.adapter = adapter
        tokenstr = prefs[Constants.token]
        refresher?.setOnRefreshListener {
            tokenstr?.let{
                bookmarksViewModel.fetchData(tokenstr!!)
            }
        }

        initializeUI()
    }

    private fun initializeUI(){
        bookmarksViewModel = ViewModelProviders.of(this).get(BookmarksViewModel::class.java)
        bookmarksViewModel.getBookmarkData().observe(this, Observer<List<BookmarkWithPhotos>> { eventList ->
            updateData(eventList)
        })
        bookmarksViewModel.getLoader().observe(this, Observer<Boolean> { loading ->
            refresher?.isRefreshing = loading
        })
        bookmarksViewModel.getErrors().observe(this, Observer<String> { errorMsg ->
            setError(errorMsg)
        })

        tokenstr?.let{
            bookmarksViewModel.fetchData(tokenstr!!)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_bookmark, menu)
        return true
    }


    private fun updateData(data: List<BookmarkWithPhotos>){
        eventlist = data
        adapter.mBookmarks.clear()
        adapter.mBookmarks.addAll(data)
        adapter.notifyDataSetChanged()
    }

    private fun setError(msg:String){
        showToast(msg, Toast.LENGTH_LONG)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val position: Int
        try {
            position = adapter.findPosition()
            val event = eventlist[position]
            tokenstr?.let {
                bookmarksViewModel.deleteBookmarkById(event.event?.id!!,tokenstr!!)
            }

        } catch (e: Exception) {
            return super.onContextItemSelected(item)
        }

        return false
    }

}
