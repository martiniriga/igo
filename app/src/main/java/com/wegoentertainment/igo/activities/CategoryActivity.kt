package com.wegoentertainment.igo.activities

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.adapters.CategoryViewAdapter
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.viewmodels.EventViewModel
import kotlinx.android.synthetic.main.activity_category.*

class CategoryActivity : BaseActivity() {
    private lateinit var eventViewModel: EventViewModel
    private var category:String? = null
    private var adapter: CategoryViewAdapter = CategoryViewAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        category = intent.getStringExtra("category")
        initializeUI()
    }

    private fun initializeUI() {
        eventViewModel = ViewModelProviders.of(this).get(EventViewModel::class.java)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        category?.let{
            txtcategory.text = category
            Log.v("eventList","$category category")
            eventViewModel.getCategories().observe(this, Observer<List<String>> { eventList ->
                Log.v("eventList","$eventList categories")
            })
            eventViewModel.getEventByCategory(category!!).observe(this, Observer<List<EventWithPhotos>> { eventList ->
                Log.v("eventList","${eventList.count()} events")
                adapter.mEvents.clear()
                adapter.mEvents.addAll(eventList)
                adapter.notifyDataSetChanged()
            })
        }
    }
}
