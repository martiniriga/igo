package com.wegoentertainment.igo.activities


import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.adapters.MainViewPagerAdapter
import kotlinx.android.synthetic.main.activity_explore.*


class ExploreActivity : BaseActivity() {

    private var mainViewPagerAdapter: MainViewPagerAdapter? = null
    private lateinit var tabLayout: TabLayout

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_explore -> {
                //already here
            }

            R.id.navigation_bookmarks -> {
                val intent2 = Intent(this@ExploreActivity, BookmarksActivity::class.java)
                startActivity(intent2)
            }

            R.id.navigation_profile -> {
                val intent3 = Intent(this@ExploreActivity, ProfileActivity::class.java)
                startActivity(intent3)
            }
        }
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explore)
        mainViewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
        container.adapter = mainViewPagerAdapter
        container.offscreenPageLimit = 3
        tabLayout = findViewById(R.id.tabs)
        tabLayout.setupWithViewPager(container)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId

        if (id == R.id.search) {
            onSearchRequested()
            return true
        }else if(id == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}


