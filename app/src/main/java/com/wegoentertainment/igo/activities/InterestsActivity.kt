package com.wegoentertainment.igo.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.wegoentertainment.igo.adapters.InterestAdapter
import com.wegoentertainment.igo.models.Category
import com.wegoentertainment.igo.models.Interest
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.PreferenceHelper.get
import com.wegoentertainment.igo.utils.showToast
import com.wegoentertainment.igo.viewmodels.CategoryViewModel
import kotlinx.android.synthetic.main.activity_interests.*


class InterestsActivity : BaseActivity() {
    private lateinit var categoryViewModel: CategoryViewModel
    private var selectedInterests:ArrayList<Category> = ArrayList()
    private var adapter: InterestAdapter = InterestAdapter{category:Category -> selectCategory(category)}

    private fun selectCategory(category: Category) {
        if(selectedInterests.contains(category)){
            selectedInterests.remove(category)
        }else{
            selectedInterests.add(category)
            adapter.mCategories.remove(category)
            adapter.notifyDataSetChanged()
        }
        when {
            selectedInterests.count()<4 -> progressChoice.progress = 33 * selectedInterests.count()
            selectedInterests.count() == 0 -> progressChoice.progress = 0
            else -> {
                progressChoice.progress = 100
                btnContinue.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.wegoentertainment.igo.R.layout.activity_interests)
        initializeUI()
    }

    private fun initializeUI(){
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        categoryViewModel.getCategoryData().observe(this, Observer<List<Category>> { categoryList ->
            updateData(categoryList)
        })
        categoryViewModel.getInterests().observe(this, Observer<List<Interest>> { interestList ->
            updateInterests(interestList)
        })
        categoryViewModel.getErrors().observe(this, Observer<String> { errorMsg ->
            setError(errorMsg)
        })
        categoryViewModel.getLoader().observe(this, Observer<Boolean> { loading ->
            if(loading){
                showProgressDialog()
            }else{
                hideProgressDialog()
            }
        })
        val tokenstr:String? = prefs[Constants.token]
        tokenstr?.let{
            categoryViewModel.fetchData(tokenstr)
        }
        if(tokenstr == null){
            categoryViewModel.fetchData("")
        }
        val staggeredGridLayoutManager = StaggeredGridLayoutManager(4, LinearLayoutManager.VERTICAL)
        recyclerView.layoutManager = staggeredGridLayoutManager
        recyclerView.adapter = adapter
        btnContinue.setOnClickListener { sendInterests() }

    }

    private fun sendInterests() {
        goToHome()
    }

    private fun goToHome() {
        val intent = Intent(this,ExploreActivity::class.java)
        startActivity(intent)
        finish()
    }


    private fun updateInterests(interestList: List<Interest>) {
        if(interestList.count()>2){
            btnContinue.visibility = View.VISIBLE
        }else{
            btnContinue.visibility = View.INVISIBLE
        }
    }

    private fun setError(msg:String){
        showToast(msg)
    }

    private fun updateData(data: List<Category>){
        adapter.mCategories.clear()
        adapter.mCategories.addAll(data)
        adapter.notifyDataSetChanged()
    }
}
