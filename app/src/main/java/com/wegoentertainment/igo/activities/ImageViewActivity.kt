package com.wegoentertainment.igo.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.wegoentertainment.igo.R

class ImageViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
    }
}
