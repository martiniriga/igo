package com.wegoentertainment.igo.activities


import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.adapters.ViewPagerAdapter
import com.wegoentertainment.igo.db.BookmarkWithPhotos
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.utils.Constants
import com.wegoentertainment.igo.utils.Helpers
import com.wegoentertainment.igo.utils.PreferenceHelper.get
import com.wegoentertainment.igo.utils.PreferenceHelper.set
import com.wegoentertainment.igo.utils.showToast
import com.wegoentertainment.igo.viewmodels.BookmarksViewModel
import com.wegoentertainment.igo.viewmodels.EventViewModel
import com.wegoentertainment.igo.viewmodels.LoginViewModel
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.android.synthetic.main.content_event_detail.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList


class EventDetailActivity : BaseActivity() {
    private var source:String = "event"
    private var currentPage = 0
    private var cost: Int = 0
    private var eventId:Int = 0
    private var charge: String = ""
    private var emailStr: String? = null
    private var contactStr: String? = null
    private var coordinateStr: String? = null
    private var linkStr: String? = null
    private var venueStr: String? = null
    private var tokenstr:String? = null
    private val CALL_REQ = 30
    private var uri: Uri? = null
    private var imageArray: ArrayList<String> = ArrayList()
    private var imageViewsDots: ArrayList<ImageView> = ArrayList()
    private lateinit var wrapper:ContextWrapper
    private var loginViewModel: LoginViewModel = LoginViewModel()
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var eventViewModel: EventViewModel
    private lateinit var bookmarksViewModel: BookmarksViewModel
    private lateinit var evPhoto:EventWithPhotos
    private lateinit var bp: BookmarkWithPhotos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)
        source = intent.getStringExtra("source")
        eventId = intent.getIntExtra("eventId",0)
        tokenstr = prefs[Constants.token]
        if(source == "event"){
            initializeGetEvent()
        }else{
            initializeGetBookmark()
        }
        setUpUI()
    }

    private fun setUpUI() {
        imgBtnAddBookmark.setOnClickListener {
            if(tokenstr == null){
                addBookmarkDialog()
            }else{
                loginViewModel.addCollection(eventId.toString(),tokenstr!!)
            }
        }
        map.setOnClickListener { mymapIntent() }
        link.setOnClickListener { setLink() }
        share.setOnClickListener{ shareEvent(bp) }
        setContact()
        setEmail()
        setUpPager()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.server_client_id))
            .requestScopes(Scope("profile"))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
        authSetUp()
    }

    private fun authSetUp(){
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        loginViewModel.getTokenFromServer().observe(this, Observer { token ->
            token?.let{
                prefs[Constants.token] = token
                loginViewModel.addCollection(eventId.toString(),token)
            }
        })
        loginViewModel.getErrorUpdates().observe(this, Observer { e ->
            showToast(e,Toast.LENGTH_LONG)
        })
        loginViewModel.getLoader().observe(this, Observer { loading ->
            if(loading){
                showProgressDialog()
            }else{
                hideProgressDialog()
            }
        })
        loginViewModel.getCollectionStatus().observe(this, Observer { sent ->
            if(sent){
                Helpers.pushAppointmentsToCalender(this,evPhoto)
                bookmarksViewModel.addBookmark(evPhoto)
                showToast("Event successfully added to collection",Toast.LENGTH_LONG)
            }else{
                showToast("Event failed to be added to your collection",Toast.LENGTH_LONG)
            }
        })
    }

    private fun setUpPager(){
        val viewPagerAdapter = ViewPagerAdapter(this, imageArray)
        viewPager.adapter = viewPagerAdapter

        val handler = Handler()
        val update = Runnable {
            if (imageArray.count() == currentPage) {
                currentPage = 0
            }
            viewPager.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 4000)



        val dotsCount = viewPagerAdapter.count
        imageViewsDots = ArrayList(dotsCount)

        for (i in 0 until dotsCount) {
            imageViewsDots[i] = ImageView(this)
            imageViewsDots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.non_active_dot))

            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.setMargins(8, 0, 8, 0)

            sliderFooter.addView(imageViewsDots[i], layoutParams)
        }

        if (imageArray.size > 1) {
            imageViewsDots[0].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dot))
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until dotsCount) {
                    imageViewsDots[i].setImageDrawable(
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.non_active_dot
                        )
                    )

                }

                imageViewsDots[position].setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.active_dot
                    )
                )

            }
            override fun onPageScrollStateChanged(state: Int) {

            }
        })//--------------------------------./Implemented methods-------------------------------------//


    }

    private fun initializeGetBookmark() {
        bookmarksViewModel = ViewModelProviders.of(this).get(BookmarksViewModel::class.java)
        bookmarksViewModel.getBookmarkById(eventId).observe(this, Observer { b ->
            bp = b
            b?.photos?.let{
                imageArray = ArrayList(b.photos!!.size)
                for(p in b.photos!!){
                    imageArray.add(p.photo)
                }
            }
            txtdescription.text = b.event?.description
            eventtitle.text = b.event?.title
            venueStr = b.event?.venue
            venue.text = venueStr
            b.event?.price?.let{
                cost =  b.event?.price!!
            }
            charge = if (cost != 0) {
                getString(R.string.charges) + String.format(getString(R.string.kshs), Helpers.formatAmount(cost))
            }else{
                getString(R.string.charges) + getString(R.string.free)
            }
            txtcharges.text = charge
            b.event?.contact_email?.let{
                email.visibility = View.VISIBLE
                emailStr = b.event?.contact_email
            }
            b.event?.contact_phone?.let{
                call.visibility = View.VISIBLE
                contactStr = b.event?.contact_phone
            }
            b?.event?.venue_coordinates?.let{
                map.visibility = View.VISIBLE
                coordinateStr = b.event?.venue_coordinates
            }
            b.event?.share_url?.let{
                link.visibility = View.VISIBLE
                linkStr = b.event?.share_url
            }
            txttime.text = "${Helpers.formatDateTime(b.event?.start_time!!)} - ${Helpers.formatDateTime(b.event?.end_time!!)}"

            Glide.with(this)
                .load(b.photos?.get(0)?.photo)
                .into(backdrop)
        })
    }

    private fun shareEvent(b:BookmarkWithPhotos?) {
        saveimg()
        getimg()
        val shareBody = String.format(getString(R.string.check_event), getString(R.string.playstore))
        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, b?.event?.title)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)
        sharingIntent.type = "image/jpeg"
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.sharevia)))
    }

    private fun saveimg(){
        wrapper = ContextWrapper(applicationContext)

        // Initializing a new file
        // The below line return a directory in internal storage
        var file = wrapper.getDir("images", Context.MODE_PRIVATE)
        // Create a file to save the image
        file = File(file, "image.jpg")
        Helpers.getBitmapFromView(backdrop,this) { bitmap->
            try {
                val stream = FileOutputStream(file) // overwrites this image every time
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                // Flush the stream
                stream.flush()
                // Close stream
                stream.close()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getimg(){
        val imagePath = wrapper.getDir("images", Context.MODE_PRIVATE)
        val newFile = File(imagePath, "image.jpg")
        uri = FileProvider.getUriForFile(applicationContext, "com.wegoentertainment.igo.fileprovider", newFile)
    }

    private fun setLink() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(linkStr)
        startActivity(i)
    }

    private fun mymapIntent() {
        val gmmIntentUri = Uri.parse("geo:$coordinateStr?q=${Uri.encode(venueStr)}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        if (mapIntent.resolveActivity(packageManager) != null) {
            startActivity(mapIntent)
        }
    }

    private fun initializeGetEvent() {
        eventViewModel = ViewModelProviders.of(this).get(EventViewModel::class.java)
        eventViewModel.getEventById(eventId).observe(this, Observer { b ->
            evPhoto = b
            bp = BookmarkWithPhotos()
            bp.event?.title = b.event?.title!!
            b?.photos?.let{
                imageArray = ArrayList(b.photos!!.size)
                for(p in b.photos!!){
                    imageArray.add(p.photo)
                }
            }
            txtdescription.text = b.event?.description
            eventtitle.text = b.event?.title
            venueStr = b.event?.venue
            venue.text = venueStr
            b.event?.price?.let{
                cost =  b.event?.price!!
            }
            charge = if (cost != 0) {
                getString(R.string.charges) + String.format(getString(R.string.kshs), Helpers.formatAmount(cost))
            }else{
                getString(R.string.charges)  + getString(R.string.free)
            }
            b.event?.contact_email?.let{
                email.visibility = View.VISIBLE
                emailStr = b.event?.contact_email
                setEmail()
            }
            b.event?.contact_phone?.let{
                call.visibility = View.VISIBLE
                contactStr = b.event?.contact_phone
            }
            b?.event?.venue_coordinates?.let{
                map.visibility = View.VISIBLE
                coordinateStr = b.event?.venue_coordinates
            }
            txtcharges.text = charge
            txttime.text = "${Helpers.formatDateTime(b.event?.start_time!!)} - ${Helpers.formatDateTime(b.event?.end_time!!)}"
            Glide.with(this)
                .load(b.photos?.get(0)?.photo)
                .into(backdrop)
        })
    }

    private fun setContact() {
        call.setOnClickListener {
            if (Helpers.isCallPermissionGranted(this)) {
                makeCall()
            } else {
                Helpers.giveUserCallInfo(this)
            }
        }
    }

    private fun makeCall(){
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contactStr!!))
        startActivity(intent)
    }

    private fun setEmail() {
        email.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + emailStr!!))
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CALL_REQ -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted
                    makeCall()
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
                return
            }
        }
    }

    private fun googleSignUp():Boolean {
        try {
            val account = GoogleSignIn.getLastSignedInAccount(applicationContext)
            account?.idToken?.let{
                loginViewModel.sendGoogleToken(account.idToken!!)
                return true
            }

        } catch (e: Exception) {
            Log.w("EventDetailActivity", "signInResult:failed code=" + e.message)
            showToast("Google sign in failed", Toast.LENGTH_LONG)
        }
        return false
    }


    private fun addBookmarkDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.bookmark))
            .setMessage(getString(R.string.sign_up))
            .setPositiveButton(android.R.string.yes) { dialog, _ ->
                val tokenstr:String? = prefs[Constants.token]
                if(tokenstr == null) {
                    val loggedIn = googleSignUp()
                    if (!loggedIn) {
                        val signInIntent = googleSignInClient.signInIntent
                        startActivityForResult(signInIntent, RC_SIGN_IN)
                    }
                }else{
                    requestCalendarPermissions()
                }
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.no
            ) { dialog, _ -> dialog.dismiss() }
            .setIcon(android.R.drawable.ic_dialog_info)
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                loginViewModel.handleSignInResult(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("Login", "Google sign in failed", e)
                showToast("Google sign in failed")
            }
        }
    }

    private fun requestCalendarPermissions() {
        if (!Helpers.isCalendarPermissionGranted(this)) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_CALENDAR),
                MY_CAL_WRITE_REQ
            )
        } else {
            tokenstr?.let{
                loginViewModel.addCollection(eventId.toString(),tokenstr!!)
            }

        }
    }

    companion object {
        private val RC_SIGN_IN = 9001
        private var MY_CAL_WRITE_REQ = 42
    }

}
