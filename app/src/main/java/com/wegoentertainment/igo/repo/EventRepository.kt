package com.wegoentertainment.igo.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wegoentertainment.igo.MyApp
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.models.Photo
import com.wegoentertainment.igo.models.gson.eventData.EventsResponse
import com.wegoentertainment.igo.services.ApiService
import com.wegoentertainment.igo.services.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback

class EventRepository {

    private val mError = MutableLiveData<String>()
    private val dataloader =  MutableLiveData<Boolean>()

    fun getEvents(): LiveData<List<EventWithPhotos>> {
        return MyApp.database!!.eventDao().getEventsWithPhotos()
    }

    fun getCategories(): LiveData<List<String>> {
        return MyApp.database!!.eventDao().getcategories()
    }

    fun getEventById(id:Int): LiveData<EventWithPhotos> {
        return MyApp.database!!.eventDao().getDistinctEventWithPhotos(id)
    }

    fun getEventByCategory(name: String): LiveData<List<EventWithPhotos>> {
        return MyApp.database!!.eventDao().getEventByCategory(name)
    }


    fun getLoader()  = dataloader as LiveData<Boolean>

    fun setLoader(loading:Boolean){
        dataloader.value = loading
    }

    fun setError(msg:String){
        mError.value = msg
    }

    fun getErrors()  = mError as LiveData<String>


    fun fetchData(token:String){
        setLoader(true)
        val apiService = ServiceBuilder.buildService(ApiService::class.java)
        val call: Call<EventsResponse> = apiService.getEvents("Bearer $token")
        call.enqueue(object : Callback<EventsResponse> {
            override fun onResponse(call: Call<EventsResponse>, response: retrofit2.Response<EventsResponse>) {
                val eventlist = response.body()?.data?.events
                Log.d("eventlist","count ${eventlist!!.count()}")
                eventlist.let{
                    val photolist = ArrayList<Photo>()
                    for(event in eventlist){
                        val photoArray = event.photos
                        for (photo in photoArray){
                            photo.eventId = event.id
                            photolist.add(photo)
                        }
                    }
                    Thread(Runnable {
                        MyApp.database!!.eventDao().updateAll(eventlist,photolist)
                    }).start()
                    setLoader(false)
                }
            }
            override fun onFailure(call: Call<EventsResponse>?, t: Throwable?) {
                setLoader(false)
                setError("Could not fetch the events")
            }
        })
    }

}