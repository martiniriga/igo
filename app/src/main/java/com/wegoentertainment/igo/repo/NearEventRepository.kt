package com.wegoentertainment.igo.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wegoentertainment.igo.MyApp
import com.wegoentertainment.igo.db.CloseEventWithPhotos
import com.wegoentertainment.igo.models.Photo
import com.wegoentertainment.igo.models.gson.eventData.EventsCloseResponse
import com.wegoentertainment.igo.services.ApiService
import com.wegoentertainment.igo.services.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback

class NearEventRepository {

    private val mError = MutableLiveData<String>()
    private val dataloader =  MutableLiveData<Boolean>()

    fun getEvents(): LiveData<List<CloseEventWithPhotos>> {
        return MyApp.database!!.closeEventDao().getCloseEvents()
    }


    fun getEventById(id:Int): LiveData<CloseEventWithPhotos> {
        return MyApp.database!!.closeEventDao().getDistinctCloseEventsById(id)
    }


    fun getLoader()  = dataloader as LiveData<Boolean>

    fun setLoader(loading:Boolean){
        dataloader.value = loading
    }

    fun setError(msg:String){
        mError.value = msg
    }

    fun getErrors()  = mError as LiveData<String>


    fun fetchData(token:String,latlong:String){
        setLoader(true)
        val apiService = ServiceBuilder.buildService(ApiService::class.java)
        val call: Call<EventsCloseResponse> = apiService.getCloseEvents("Bearer $token",latlong)
        call.enqueue(object : Callback<EventsCloseResponse> {
            override fun onResponse(call: Call<EventsCloseResponse>, response: retrofit2.Response<EventsCloseResponse>) {
                val eventlist = response.body()?.data?.events
                Log.d("neareventlist","count ${eventlist!!.count()}")
                eventlist.let{
                    val photolist = ArrayList<Photo>()
                    for(event in eventlist){
                        val photoArray = event.photos
                        for (photo in photoArray){
                            photo.eventId = event.id
                            photolist.add(photo)
                        }
                    }
                    Thread(Runnable {
                        MyApp.database!!.closeEventDao().updateAll(eventlist,photolist)
                    }).start()
                    setLoader(false)
                }
            }
            override fun onFailure(call: Call<EventsCloseResponse>?, t: Throwable?) {
                setLoader(false)
                setError("Could not fetch the events")
            }
        })
    }

}