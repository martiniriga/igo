package com.wegoentertainment.igo.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wegoentertainment.igo.MyApp
import com.wegoentertainment.igo.models.Category
import com.wegoentertainment.igo.models.Interest
import com.wegoentertainment.igo.models.gson.categoryData.CategoryResponse
import com.wegoentertainment.igo.services.ApiService
import com.wegoentertainment.igo.services.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback

class CategoryRepository {

    private val mError = MutableLiveData<String>()
    private val dataloader =  MutableLiveData<Boolean>()

    fun getCategories(): LiveData<List<Category>> {
        return MyApp.database!!.categoryDao().getCategories()
    }

    fun getInterests(): LiveData<List<Interest>> {
        return MyApp.database!!.categoryDao().getInterests()
    }

    fun getLoader()  = dataloader as LiveData<Boolean>

    fun setLoader(loading:Boolean){
        dataloader.value = loading
    }

    fun setError(msg:String){
        mError.value = msg
    }

    fun getErrors()  = mError as LiveData<String>


    fun fetchData(token:String){
        setLoader(true)
        val apiService = ServiceBuilder.buildService(ApiService::class.java)
        val call: Call<CategoryResponse> = apiService.getCategories("Bearer $token")
        call.enqueue(object : Callback<CategoryResponse> {
            override fun onResponse(call: Call<CategoryResponse>, response: retrofit2.Response<CategoryResponse>) {
                setLoader(false)
                val categorylist = response.body()?.data?.categories
                categorylist?.let{
                    Thread(Runnable {
                        MyApp.database!!.categoryDao().deleteAll()
                        MyApp.database!!.categoryDao().insertAll(categorylist)

                    }).start()
                }
            }
            override fun onFailure(call: Call<CategoryResponse>?, t: Throwable?) {
                setLoader(false)
                setError("Could not fetch the Categories")
            }
        })
    }
}