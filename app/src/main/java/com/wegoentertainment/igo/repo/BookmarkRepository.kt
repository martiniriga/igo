package com.wegoentertainment.igo.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.wegoentertainment.igo.MyApp
import com.wegoentertainment.igo.db.BookmarkWithPhotos
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.models.BookmarkPhoto
import com.wegoentertainment.igo.models.gson.addBookmarkData.AddBookmarkQuery
import com.wegoentertainment.igo.models.gson.bookmarkData.BookmarksResponse
import com.wegoentertainment.igo.services.ApiService
import com.wegoentertainment.igo.services.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback

class BookmarkRepository {

    private val mError = MutableLiveData<String>()
    private val dataloader =  MutableLiveData<Boolean>()

    fun getBookmarks(): LiveData<List<BookmarkWithPhotos>> {
        return MyApp.database!!.bookmarkDao().getBookmarkWithPhotos()
    }
    fun getBookmarkById(id:Int):LiveData<BookmarkWithPhotos>{
        return MyApp.database!!.bookmarkDao().getDistinctBookmarkWithPhotos(id)
    }

    fun getLoader()  = dataloader as LiveData<Boolean>

    fun setLoader(loading:Boolean){
        dataloader.value = loading
    }

    fun setError(msg:String){
        mError.value = msg
    }

    fun getErrors()  = mError as LiveData<String>

    fun addBookmark(b: EventWithPhotos){
        val evJson = Gson().toJson(b,EventWithPhotos::class.java)
        val bookmarkWithPhotos = Gson().fromJson(evJson,BookmarkWithPhotos::class.java)
        val photoArray = bookmarkWithPhotos.photos
        val photolist = ArrayList<BookmarkPhoto>()
        if (photoArray!!.isNotEmpty()){
            for (photo in photoArray){
                photo.eventId = bookmarkWithPhotos.event!!.id
                val bookmarkPhoto = BookmarkPhoto(photo.id,photo.description,photo.photo,photo.eventId)
                photolist.add(bookmarkPhoto)
            }
        }
        Thread(Runnable {
            MyApp.database!!.bookmarkDao().insertEvent(bookmarkWithPhotos.event!!)
            MyApp.database!!.bookmarkDao().insertAllPhotos(photolist)
        }).start()
    }


    fun fetchData(token:String){
        setLoader(true)
        val apiService = ServiceBuilder.buildService(ApiService::class.java)
        val call: Call<BookmarksResponse> = apiService.getBookmarks("Bearer $token")
        call.enqueue(object : Callback<BookmarksResponse> {
            override fun onResponse(call: Call<BookmarksResponse>, response: retrofit2.Response<BookmarksResponse>) {
                val eventlist = response.body()?.data?.events
                eventlist?.let{
                    val photolist = ArrayList<BookmarkPhoto>()
                    for(event in eventlist){
                        val photoArray = event.photos
                        for (photo in photoArray){
                            photo.eventId = event.id
                            photolist.add(photo)
                        }
                    }
                    Thread(Runnable {
                        MyApp.database!!.bookmarkDao().updateAll(eventlist,photolist)
                    }).start()
                    setLoader(false)
                }
            }
            override fun onFailure(call: Call<BookmarksResponse>?, t: Throwable?) {
                setLoader(false)
                setError("Could not fetch any bookmarks")
            }
        })
    }

    fun removeBookmark(event_id:Int,token:String){
        setLoader(true)
        val apiService = ServiceBuilder.buildService(ApiService::class.java)
        val call: Call<AddBookmarkQuery> = apiService.removeBookmark(event_id,"Bearer $token")
        call.enqueue(object : Callback<AddBookmarkQuery> {
            override fun onResponse(call: Call<AddBookmarkQuery>, response: retrofit2.Response<AddBookmarkQuery>) {
                if(response.code() == 401){
                    setError("Invalid credentials")
                }else {
                    Thread(Runnable {
                        MyApp.database!!.bookmarkDao().deleteBookmarkById(event_id)
                    }).start()
                    setError(response.body()!!.data)
                }
                setLoader(false)
            }
            override fun onFailure(call: Call<AddBookmarkQuery>?, t: Throwable?) {
                setLoader(false)
                setError("Could not remove the bookmark")
            }
        })
    }

}