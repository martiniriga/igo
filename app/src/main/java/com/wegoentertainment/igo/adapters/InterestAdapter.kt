package com.wegoentertainment.igo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.holders.InterestViewHolder
import com.wegoentertainment.igo.models.Category
import java.util.*

class InterestAdapter(private val clickListener: (Category) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mCategories = ArrayList<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterestViewHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.interest_row, parent, false)
        InterestViewHolder(layoutView)
        return InterestViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as InterestViewHolder).bindData(mCategories[position],clickListener)
    }

    override fun getItemCount(): Int {
        return mCategories.size
    }

}