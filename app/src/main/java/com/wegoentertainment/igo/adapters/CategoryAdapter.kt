package com.wegoentertainment.igo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.holders.CategoryViewHolder
import com.wegoentertainment.igo.models.Category
import java.util.*

class CategoryAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mCategories = ArrayList<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.category_row, parent, false)
        CategoryViewHolder(layoutView)
        return CategoryViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CategoryViewHolder).bindData(mCategories[position])
    }

    override fun getItemCount(): Int {
        return mCategories.size
    }

}