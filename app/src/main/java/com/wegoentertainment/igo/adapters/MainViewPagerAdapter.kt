package com.wegoentertainment.igo.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.wegoentertainment.igo.fragments.CategoryFragment
import com.wegoentertainment.igo.fragments.NearMeFragment
import com.wegoentertainment.igo.fragments.WeekFragment

class MainViewPagerAdapter internal constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment = WeekFragment()
        when (position) {
            0 -> fragment = WeekFragment()
            1 -> fragment = CategoryFragment()
            2 -> fragment = NearMeFragment()
        }

        return fragment
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title:String? = null
        when (position) {
            0 -> title = "THIS WEEK"
            1 -> title = "CATEGORIES"
            2 -> title = "NEAR ME"
        }
        return title
    }
}