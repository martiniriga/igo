package com.wegoentertainment.igo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.db.BookmarkWithPhotos
import com.wegoentertainment.igo.holders.BookmarkViewHolder
import java.util.*

class BookmarkAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mBookmarks = ArrayList<BookmarkWithPhotos>()
    var position: Int = 0

    fun findPosition(): Int {
        return position
    }

    private fun assignPosition(position: Int) {
        this.position = position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookmarkViewHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.bookmark_row, parent, false)
        BookmarkViewHolder(layoutView)
        return BookmarkViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as BookmarkViewHolder).bindData(mBookmarks[position])
        holder.itemView.setOnLongClickListener {
            assignPosition(position)
            false
        }
    }

    override fun getItemCount(): Int {
        return mBookmarks.size
    }



}