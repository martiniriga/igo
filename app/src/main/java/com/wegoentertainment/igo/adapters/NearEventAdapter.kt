package com.wegoentertainment.igo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.db.CloseEventWithPhotos
import com.wegoentertainment.igo.holders.NearEventHolder
import java.util.*

class NearEventAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mEvents = ArrayList<CloseEventWithPhotos>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NearEventHolder {
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.near_row, parent, false)
        NearEventHolder(layoutView)
        return NearEventHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NearEventHolder).bindData(mEvents[position])
    }


    override fun getItemCount(): Int {
        return mEvents.size
    }


}