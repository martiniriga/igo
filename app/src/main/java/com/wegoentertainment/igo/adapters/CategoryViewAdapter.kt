package com.wegoentertainment.igo.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.holders.CategoryPhotoViewHolder
import java.util.*

class CategoryViewAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var context: Context? = null

    val mEvents = ArrayList<EventWithPhotos>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.category_photo_row, parent, false)
        return CategoryPhotoViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       (holder as CategoryPhotoViewHolder).bindData(mEvents[position])
    }

    override fun getItemCount(): Int {
        return mEvents.size
    }

}