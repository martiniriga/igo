package com.wegoentertainment.igo.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.holders.EventHeaderViewHolder
import com.wegoentertainment.igo.holders.ItemViewHolder
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class EventAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var context: Context? = null
    private val TYPE_HEADER = 0
    private val TYPE_ITEM = 1

    val mEvents = ArrayList<EventWithPhotos>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return if (viewType == TYPE_HEADER) {
            val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.weekheader, parent, false)
            EventHeaderViewHolder(layoutView)
        } else {
            val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.week_photo, parent, false)
            ItemViewHolder(layoutView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemType = getItemViewType(position)
        if (itemType == TYPE_HEADER) {
            (holder as EventHeaderViewHolder).bindData(mEvents[position])
        } else if (itemType == TYPE_ITEM) {
            (holder as ItemViewHolder).bindData(mEvents[position])
        }
    }

    override fun getItemCount(): Int {
        return mEvents.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (!isPositionHeader(position))
            TYPE_HEADER
        else {
            TYPE_ITEM
        }
    }

    private fun isPositionHeader(position: Int): Boolean {
        var diffdate: Boolean? = false
        if (position > 0) {
            val weekPhoto = mEvents[position].event
            val dformat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val fmt = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
            val previousweekPhoto = mEvents[position - 1].event
            try {
                val currentItemDate = dformat.parse(weekPhoto?.start_time)
                val previousItemDate = dformat.parse(previousweekPhoto?.start_time)
                diffdate = fmt.format(currentItemDate) == fmt.format(previousItemDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }
        return diffdate!!
    }


}