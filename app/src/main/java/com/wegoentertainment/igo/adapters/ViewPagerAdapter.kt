package com.wegoentertainment.igo.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.activities.ImageViewActivity

class ViewPagerAdapter (private val mContext: Context, private val images: ArrayList<String>) : PagerAdapter() {

    override fun getCount(): Int {
        return images.size
    }

    private var mLayoutInflater: LayoutInflater? = null


    //------------------------------------isViewFromObject----------------------------------------//
    override fun isViewFromObject(@NonNull view: View, @NonNull `object`: Any): Boolean {
        return view === `object`
    }//------------------------------------./isViewFromObject-----------------------------------------//


    //------------------------------------Object instantiateItem-----------------------------------------//
    @NonNull
    override fun instantiateItem(@NonNull container: ViewGroup, position: Int): Any {

        mLayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view:View = mLayoutInflater!!.inflate(R.layout.image_slider_layout, null)

        val imageView = view.findViewById(R.id.sliderImage) as ImageView
        Glide.with(mContext)
            .asBitmap()
            .load(images[position])
            .into(imageView)


        imageView.setOnClickListener(View.OnClickListener {
            val passImageUrlIntent = Intent(mContext, ImageViewActivity::class.java)

            val bundle = Bundle()
            bundle.putString("ImgURL", images[position])
            passImageUrlIntent.putExtras(bundle)
            mContext.startActivity(passImageUrlIntent)
        })

        val viewPager = container as ViewPager
        viewPager.addView(view, 0)


        return view
    }//------------------------------------./Object instantiateItem-----------------------------------------//


    //------------------------------------destroyItem----------------------------------------//
    override fun destroyItem(@NonNull container: ViewGroup, position: Int, @NonNull `object`: Any) {
        val mViewPager = container as ViewPager
        val mView = `object` as View

        mViewPager.removeView(mView)
    } //------------------------------------./destroyItem----------------------------------------//


}//------------------------------------./ViewPagerAdapter extends PagerAdapter-----------------------------------------//
