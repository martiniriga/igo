package com.wegoentertainment.igo.utils

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.provider.CalendarContract
import android.text.TextUtils
import android.view.PixelCopy
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.wegoentertainment.igo.R
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.utils.PreferenceHelper.get
import com.wegoentertainment.igo.utils.PreferenceHelper.set
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object Helpers {

    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun formatDate(dateStr: String, prefix: String = ""): String {
        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(dateStr)
        return prefix + SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(date)
    }

    fun formatDateTime(dateStr: String): String {
        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(dateStr)
        return SimpleDateFormat("dd MMM yy HH:mm", Locale.getDefault()).format(date)
    }


    fun getFormattedDateSimple(dateTime: Long?): String {
        val newFormat = SimpleDateFormat("dd/MM/yyyy")
        return newFormat.format(Date(dateTime!!))
    }
    fun formatShortDate(dateStr: String, prefix: String = ""): String {
        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(dateStr)
        return prefix + SimpleDateFormat("dd MMM", Locale.getDefault()).format(date)
    }

    fun formatAmount(amount: Int, prefix: String = ""): String {
        if (amount.equals(0.00)) {
            return prefix + "0"
        }
        val formatter = DecimalFormat("#,###,###")
        return prefix + formatter.format(amount)
    }

    fun isCalendarPermissionGranted(context:Context): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.WRITE_CALENDAR
        ) == PackageManager.PERMISSION_GRANTED
    }
//
//    fun displayImageRound(ctx: Context, img: ImageView, url: String) {
//        try {
//            Glide.with(ctx).load(url).asBitmap()
//                .placeholder(ContextCompat.getDrawable(ctx, R.drawable.ic_account_circle_black_24dp))
//                .centerCrop()
//                .dontAnimate()
//                .into(object : BitmapImageViewTarget(img) {
//                override fun setResource(resource: Bitmap) {
//                    val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(ctx.resources, resource)
//                    circularBitmapDrawable.isCircular = true
//                    img.setImageDrawable(circularBitmapDrawable)
//                }
//            })
//        } catch (e: Exception) {
//        }
//
//    }


    fun upperCaseWords(sentence: String): String {
        val words = sentence.replace("\\s+".toRegex(), " ").trim { it <= ' ' }.split(" ".toRegex())
            .dropLastWhile { it.isEmpty() }
            .toTypedArray()
        var newSentence = ""
        for (word in words) {
            for (i in 0 until word.length)
                newSentence += when {
                    i == 0 -> word.substring(i, i + 1).toUpperCase()
                    i != word.length - 1 -> word.substring(i, i + 1).toLowerCase()
                    else -> word.substring(
                        i,
                        i + 1
                    ).toLowerCase() + " "
                }
        }

        return newSentence
    }

    fun isCallPermissionGranted(context: Context): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.CALL_PHONE
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun requestCallPermission(activity: Activity) {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CALL_PHONE), 30)
    }

    fun giveUserCallInfo(activity:Activity){
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(activity.getString(R.string.call_permission)) // Your own title
        builder.setMessage(activity.getString(R.string.allow_call)) // Your own message

        builder.setPositiveButton(android.R.string.ok) { dialog, _ ->
            dialog.dismiss()
            Helpers.requestCallPermission(activity)
        }
        builder.setNegativeButton(android.R.string.cancel){ dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }

    fun getBitmapFromView(view: View, activity: Activity, callback: (Bitmap) -> Unit) {
        activity.window?.let { window ->
            val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
            val locationOfViewInWindow = IntArray(2)
            view.getLocationInWindow(locationOfViewInWindow)
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    PixelCopy.request(window,
                        Rect(
                            locationOfViewInWindow[0],
                            locationOfViewInWindow[1],
                            locationOfViewInWindow[0] + view.width,
                            locationOfViewInWindow[1] + view.height
                        ), bitmap, { copyResult ->
                        if (copyResult == PixelCopy.SUCCESS) {
                            callback(bitmap)
                        }
                        // possible to handle other result codes ...
                    },
                        Handler())
                }
            } catch (e: IllegalArgumentException) {
                // PixelCopy may throw IllegalArgumentException, make sure to handle it
                e.printStackTrace()
            }
        }
    }


    fun pushAppointmentsToCalender(curActivity: Activity, event: EventWithPhotos): Long {
        val eventUriString = "content://com.android.calendar/events"
        val eventValues = ContentValues()

        eventValues.put("calendar_id", 1) // id, We need to choose from
        // our mobile for primary
        // its 1
        eventValues.put("title", event.event?.title)
        eventValues.put("description", event.event?.description)
        eventValues.put("eventLocation", event.event?.venue)

        var startdate: Date? = null
        var enddate: Date? = null
        val dformat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            startdate = dformat.parse(event.event?.start_time)
            enddate = dformat.parse(event.event?.end_time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val startmillis = startdate!!.time
        val endmillis = enddate!!.time

        eventValues.put("dtstart", startmillis)
        eventValues.put("dtend", endmillis)

        // values.put("allDay", 1); //If it is bithday alarm or such
        // kind (which should remind me for whole day) 0 for false, 1
        // for true
        eventValues.put("eventStatus", 0) // This information is
        // sufficient for most
        // entries tentative (0),
        // confirmed (1) or canceled
        // (2):
        eventValues.put(CalendarContract.Events.EVENT_TIMEZONE, "Africa/Nairobi") //UTC/GMT +3:00
        eventValues.put(CalendarContract.Events.EVENT_END_TIMEZONE, "Africa/Nairobi")
        /*Comment below visibility and transparency  column to avoid java.lang.IllegalArgumentException column visibility is invalid error */

        /*eventValues.put("visibility", 3); // visibility to default (0),
                                        // confidential (1), private
                                        // (2), or public (3):
    eventValues.put("transparency", 0); // You can control whether
                                        // an event consumes time
                                        // opaque (0) or transparent
                                        // (1).
      */
        eventValues.put("hasAlarm", 1) // 0 for false, 1 for true

        val eventUri = curActivity.applicationContext.contentResolver.insert(Uri.parse(eventUriString), eventValues)
        val eventID = java.lang.Long.parseLong(eventUri!!.lastPathSegment!!)
        val pref = PreferenceHelper.defaultPrefs(curActivity.applicationContext)
        if (!hasCalendar(pref,eventUri.lastPathSegment))
            setCalendar(pref,event.event?.id.toString() + ":" + eventUri.lastPathSegment)
        val needReminder = true
        if (needReminder) {
            /**************** Event: Reminder(with alert) Adding reminder to event  */

            val reminderUriString = "content://com.android.calendar/reminders"

            val reminderValues = ContentValues()

            reminderValues.put("event_id", eventID)
            reminderValues.put("minutes", 480) // Default value of the
            // system. Minutes is a
            // integer
            reminderValues.put("method", 1) // Alert Methods: Default(0),
            // Alert(1), Email(2),
            // SMS(3)

            curActivity.applicationContext.contentResolver.insert(Uri.parse(reminderUriString), reminderValues)
        }

        return eventID

    }

    private fun hasCalendar(pref: SharedPreferences, cal: String?): Boolean {
        val calstring:String? = pref[Constants.calendar]
        if (calstring != null) {
            val calArray = calstring.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (s in calArray) {
                if (s == cal)
                    return true
            }
        }
        return false
    }

    private fun setCalendar(pref: SharedPreferences, cal: String) {
        var calstring:String? = pref[Constants.calendar]
        calstring = if (calstring != null) {
            "$calstring,$cal"
        } else {
            cal
        }
        pref[Constants.calendar] = calstring
    }

}