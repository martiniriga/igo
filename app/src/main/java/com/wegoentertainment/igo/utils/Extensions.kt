package com.wegoentertainment.igo.utils

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar

fun Context.showToast(message:String, duration:Int =Toast.LENGTH_SHORT){
    Toast.makeText(this,message,duration).show()

}

fun Context.isConnectedToNetwork(): Boolean {
//    return true
    //#TODO uncomment on live
    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = connectivityManager.activeNetworkInfo

    return activeNetwork != null && activeNetwork.isConnected
}

fun alert(view: View, message:String = "No internet connection", duration:Int = Snackbar.LENGTH_SHORT){
    Snackbar.make(view, message, duration).show()
}

fun <T> LiveData<T>.getDistinct(): LiveData<T> {
    val distinctLiveData = MediatorLiveData<T>()
    distinctLiveData.addSource(this, object : Observer<T> {
        private var initialized = false
        private var lastObj: T? = null
        override fun onChanged(obj: T?) {
            if (!initialized) {
                initialized = true
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            } else if ((obj == null && lastObj != null)
                || obj != lastObj) {
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            }
        }
    })
    return distinctLiveData
}

