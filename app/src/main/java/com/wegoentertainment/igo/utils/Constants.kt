package com.wegoentertainment.igo.utils

object Constants {
    const val token= "token"
    const val google_token= "google_token"
    const val name = "name"
    const val photo_url = "photo_url"
    const val verified_phone  = "verified_phone"
    const val verified_email = "verified_email"
    const val email= "email"
    const val phone_code = "phone_code"
    const val id_number = "id_number"
    const val phone = "phone"
    const val id = "id"
    const val account_type = "account_type"
    const val calendar = "calendar"


}