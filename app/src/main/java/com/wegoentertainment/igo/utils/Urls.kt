package com.wegoentertainment.igo.utils


object Urls {
    const val APP_URL = "http://10.0.2.2:8000/api/"

//    const val APP_URL = "https://wegoplaces.net/api/"


    val URL_CATEGORY = "events/category/"
    val URL_SIGN_IN_GOOGLE = "auth/google"
    val URL_ADD_DEVICE = "add/device"
    val URL_ADD_FCM = "firebase/register"
    val URL_POLL = "poll"
    val URL_DEVICE_ACTIVITY = "add/duration"
    val URL_EVENT_TRACK = "track/event"
    val URL_CATEGORY_TRACK = "track/category"
    val URL_ELEMENT_TRACK = "track/element"

    fun getUrl(url:String):String{
        return APP_URL + url
    }


}