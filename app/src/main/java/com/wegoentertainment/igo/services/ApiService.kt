package com.wegoentertainment.igo.services

import com.wegoentertainment.igo.models.gson.Login.LoginQuery
import com.wegoentertainment.igo.models.gson.addBookmarkData.AddBookmarkQuery
import com.wegoentertainment.igo.models.gson.bookmarkData.BookmarksResponse
import com.wegoentertainment.igo.models.gson.categoryData.CategoryResponse
import com.wegoentertainment.igo.models.gson.eventData.EventsCloseResponse
import com.wegoentertainment.igo.models.gson.eventData.EventsResponse
import retrofit2.Call
import retrofit2.http.*


interface ApiService {
    @FormUrlEncoded
    @POST("auth/google")
    fun googleSignIn(@Field("google_token") google_token: String): Call<LoginQuery>

    @FormUrlEncoded
    @POST("firebase/register")
    fun registerFirebase(@Field("id") firebaseToken: String): Call<LoginQuery>

    @GET("events")
    fun getEvents(@Header("Authorization") authHeader: String): Call<EventsResponse>

    @GET("categories")
    fun getCategories(@Header("Authorization") authHeader: String): Call<CategoryResponse>

    @GET("collection")
    fun getBookmarks(@Header("Authorization") authHeader: String): Call<BookmarksResponse>

    @FormUrlEncoded
    @POST("collection/add")
    fun addBookmark(@Field("event_id") event_id: String, @Header("Authorization") authHeader: String): Call<AddBookmarkQuery>

    @FormUrlEncoded
    @POST("collection/remove")
    fun removeBookmark(@Field("event_id") event_id: Int, @Header("Authorization") authHeader: String): Call<AddBookmarkQuery>

    @GET("events/closest")
    fun getCloseEvents(@Header("Authorization") authHeader: String, @Query("latlong") latlong: String): Call<EventsCloseResponse>


}