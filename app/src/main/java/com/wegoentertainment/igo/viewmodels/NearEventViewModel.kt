package com.wegoentertainment.igo.viewmodels

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wegoentertainment.igo.repo.NearEventRepository

class NearEventViewModel :ViewModel() {
    
    var currentLocation: MutableLiveData<Location>? = MutableLiveData()

    fun updateLocation(location: Location?) {
        currentLocation?.value = location
    }

    fun fetchData(token:String,latlong:String){
        eventRepository.fetchData(token,latlong)
    }

    var eventRepository: NearEventRepository = NearEventRepository()

    fun getEventData()  = eventRepository.getEvents()

    fun getErrors() = eventRepository.getErrors()

    fun getLoader() = eventRepository.getLoader()


}