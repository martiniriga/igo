package com.wegoentertainment.igo.viewmodels

import androidx.lifecycle.ViewModel
import com.wegoentertainment.igo.repo.CategoryRepository

class CategoryViewModel :ViewModel() {

    var categoryRepository: CategoryRepository = CategoryRepository()

    fun getCategoryData()  = categoryRepository.getCategories()

    fun getInterests()  = categoryRepository.getInterests()

    fun getErrors() = categoryRepository.getErrors()

    fun fetchData(token:String){
        categoryRepository.fetchData(token)
    }

    fun getLoader() = categoryRepository.getLoader()

    
}