package com.wegoentertainment.igo.viewmodels

import androidx.lifecycle.ViewModel
import com.wegoentertainment.igo.db.EventWithPhotos
import com.wegoentertainment.igo.repo.BookmarkRepository

class BookmarksViewModel :ViewModel() {

    var bookmarkRepository: BookmarkRepository = BookmarkRepository()

    fun getBookmarkData()  = bookmarkRepository.getBookmarks()

    fun getErrors() = bookmarkRepository.getErrors()

    fun fetchData(token:String){
        bookmarkRepository.fetchData(token)
    }

    fun getLoader() = bookmarkRepository.getLoader()

    fun getBookmarkById(id:Int)  = bookmarkRepository.getBookmarkById(id)

    fun addBookmark(bookmark: EventWithPhotos) = bookmarkRepository.addBookmark(bookmark)

    fun deleteBookmarkById(id:Int,token:String)  = bookmarkRepository.removeBookmark(id,token)
}