package com.wegoentertainment.igo.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.wegoentertainment.igo.models.gson.Login.LoginQuery
import com.wegoentertainment.igo.models.gson.addBookmarkData.AddBookmarkQuery
import com.wegoentertainment.igo.services.ApiService
import com.wegoentertainment.igo.services.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback

class LoginViewModel: ViewModel() {

    private var mDataApi  = MutableLiveData<String>()
    private var mError  = MutableLiveData<String>()
    private var mSent = MutableLiveData<Boolean>()
    private var apiService = ServiceBuilder.buildService(ApiService::class.java)
    private val dataloader =  MutableLiveData<Boolean>()
    

    fun getLoader()  = dataloader as LiveData<Boolean>

    fun getCollectionStatus()  = mSent as LiveData<Boolean>

    private fun setLoader(loading:Boolean){
        dataloader.value = loading
    }

    fun getTokenFromServer(): MutableLiveData<String> {
        return mDataApi
    }

    fun getErrorUpdates(): MutableLiveData<String> {
        return mError
    }

    fun sendGoogleToken(token:String) {
        setLoader(true)
        val call: Call<LoginQuery> = apiService.googleSignIn(token)

        call.enqueue(object : Callback<LoginQuery> {
            override fun onResponse(call: Call<LoginQuery>, response: retrofit2.Response<LoginQuery>) {
                setLoader(false)
                val data = response.body()
                if(response.code() == 401){
                    mError.value = "Invalid credentials"
                }else{
                    data?.let{
                        mDataApi.value = data.token
                    }
                }
                Log.e("success message: ",response.body().toString())
            }
            override fun onFailure(call: Call<LoginQuery>?, t: Throwable?) {
                setLoader(false)
                mError.value = t?.message
            }
        })
    }

    fun addCollection(eventId:String,token:String) {
        setLoader(true)
        val call: Call<AddBookmarkQuery> = apiService.addBookmark(eventId,token)

        call.enqueue(object : Callback<AddBookmarkQuery> {
            override fun onResponse(call: Call<AddBookmarkQuery>, response: retrofit2.Response<AddBookmarkQuery>) {
                setLoader(false)
                val data = response.body()
                if(response.code() == 401){
                    mError.value = "Invalid credentials"
                    mSent.value = false
                }else{
                    data?.let{
                        mError.value = data.data
                        mSent.value = true
                    }
                }
                Log.e("success message: ",response.body().toString())
            }
            override fun onFailure(call: Call<AddBookmarkQuery>?, t: Throwable?) {
                setLoader(false)
                mError.value = t?.message
                mSent.value = false
            }
        })
    }




    fun handleSignInResult(account: GoogleSignInAccount?) {
        account?.idToken?.let{ sendGoogleToken(account.idToken!!) }
    }


}