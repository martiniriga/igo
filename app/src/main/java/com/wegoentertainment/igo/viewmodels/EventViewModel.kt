package com.wegoentertainment.igo.viewmodels

import androidx.lifecycle.ViewModel
import com.wegoentertainment.igo.repo.EventRepository

class EventViewModel :ViewModel() {

    var eventRepository: EventRepository = EventRepository()

    fun getEventData()  = eventRepository.getEvents()

    fun getErrors() = eventRepository.getErrors()

    fun fetchData(token:String){
        eventRepository.fetchData(token)
    }

    fun getLoader() = eventRepository.getLoader()

    fun getEventById(Id: Int)  = eventRepository.getEventById(Id)

    fun getEventByCategory(name: String)  = eventRepository.getEventByCategory(name)


    fun getCategories()  = eventRepository.getCategories()

}