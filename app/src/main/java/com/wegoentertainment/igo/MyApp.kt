package com.wegoentertainment.igo

import android.content.Context
import androidx.multidex.MultiDexApplication
import androidx.room.Room
import com.wegoentertainment.igo.db.EventsDatabase

class MyApp: MultiDexApplication() {
    companion object {
        var database:EventsDatabase? = null

    }
    lateinit var context: Context
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(applicationContext,EventsDatabase::class.java,"events_db")
            .fallbackToDestructiveMigration().build()
        context = applicationContext
    }
}