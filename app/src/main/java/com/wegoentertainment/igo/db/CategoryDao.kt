package com.wegoentertainment.igo.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wegoentertainment.igo.models.Category
import com.wegoentertainment.igo.models.Interest
import com.wegoentertainment.igo.utils.getDistinct

@Dao
abstract class CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(categoryList: List<Category>)

    @Query("DELETE FROM categories")
    abstract fun deleteAll()

    @Query("SELECT * from categories ORDER BY id ASC")
    abstract fun getCategories(): LiveData<List<Category>>

    @Query("SELECT * from interests ORDER BY id ASC")
    abstract fun getInterests(): LiveData<List<Interest>>

    @Query("SELECT * FROM interests WHERE id=:id")
    protected abstract fun getInterests(id: Int) : LiveData<Interest>

    fun getDistinctInterests(id: Int):
            LiveData<Interest> = getInterests(id).getDistinct()

    @Insert
    abstract fun insertAllInterests(interests: List<Interest>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertInterest(interest: Interest)

    @Query("DELETE FROM interests")
    abstract fun deleteAllInterests()
}