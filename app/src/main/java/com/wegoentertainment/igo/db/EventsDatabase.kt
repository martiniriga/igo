package com.wegoentertainment.igo.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.wegoentertainment.igo.models.*


@Database(entities = [Category::class, Event::class,Photo::class, Bookmark::class,BookmarkPhoto::class,Interest::class,CloseEvent::class], version = 1)
abstract class EventsDatabase : RoomDatabase() {

    abstract fun categoryDao(): CategoryDao
    abstract fun eventDao(): EventDao
    abstract fun closeEventDao(): CloseEventDao
    abstract fun bookmarkDao(): BookmarkDao

    companion object {

        @Volatile
        private var INSTANCE: EventsDatabase? = null

        internal fun getDatabase(context: Context): EventsDatabase? {
            if (INSTANCE == null) {
                synchronized(EventsDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            EventsDatabase::class.java, "events_db"
                        )
                            .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}