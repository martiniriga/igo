package com.wegoentertainment.igo.db


import androidx.room.Embedded
import androidx.room.Relation
import com.wegoentertainment.igo.models.Event
import com.wegoentertainment.igo.models.Photo


class EventWithPhotos {
    @Embedded
    var event: Event? = null

    @Relation(parentColumn = "id", entityColumn = "eventId",entity = Photo::class)
    var photos: List<Photo>? = null
}