package com.wegoentertainment.igo.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.wegoentertainment.igo.models.CloseEvent
import com.wegoentertainment.igo.models.Photo
import com.wegoentertainment.igo.utils.getDistinct

@Dao
abstract class CloseEventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertEvent(closeEvent: CloseEvent)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(CloseEventList: List<CloseEvent>)

    @Insert
    abstract fun insertAllPhotos(photos: List<Photo>)


    @Query("DELETE FROM CloseEvents")
    abstract fun deleteAll()


    @Query("DELETE FROM CloseEvents WHERE id=:id")
    abstract fun deleteCloseEvent(id:Int)

    @Transaction
    @Query("SELECT * FROM CloseEvents WHERE id=:eventId")
    protected abstract fun getCloseEventById(eventId: Int) : LiveData<CloseEventWithPhotos>

    fun getDistinctCloseEventsById(id: Int):
            LiveData<CloseEventWithPhotos> = getCloseEventById(id).getDistinct()

    @Transaction
    @Query("SELECT * FROM events WHERE id=:eventId")
    protected abstract fun getEventWithPhotos(eventId: Int) : LiveData<CloseEventWithPhotos>

    @Transaction
    @Query("SELECT * FROM CloseEvents ORDER BY distance ASC")
    abstract fun getCloseEvents(): LiveData<List<CloseEventWithPhotos>>

    @Transaction
    open fun updateAll(eventList: List<CloseEvent>,photos: List<Photo>) {
        deleteAll()
        insertAll(eventList)
        insertAllPhotos(photos)
    }

}