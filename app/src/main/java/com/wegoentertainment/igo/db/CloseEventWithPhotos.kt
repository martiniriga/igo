package com.wegoentertainment.igo.db


import androidx.room.Embedded
import androidx.room.Relation
import com.wegoentertainment.igo.models.CloseEvent
import com.wegoentertainment.igo.models.Photo


class CloseEventWithPhotos {
    @Embedded
    var event: CloseEvent? = null

    @Relation(parentColumn = "id", entityColumn = "eventId",entity = Photo::class)
    var photos: List<Photo>? = null
}