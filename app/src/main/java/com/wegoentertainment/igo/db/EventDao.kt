package com.wegoentertainment.igo.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.wegoentertainment.igo.models.Event
import com.wegoentertainment.igo.models.Photo
import com.wegoentertainment.igo.utils.getDistinct

@Dao
abstract class EventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertEvent(event: Event)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(eventList: List<Event>)

    @Insert
    abstract fun insertAllPhotos(photos: List<Photo>)

    @Query("DELETE FROM events")
    abstract fun deleteAll()

    @Query("DELETE FROM photos")
    abstract fun deleteAllPhotos()

    @Transaction
    @Query("SELECT * FROM events WHERE id=:eventId")
    protected abstract fun getEventWithPhotos(eventId: Int) : LiveData<EventWithPhotos>

    fun getDistinctEventWithPhotos(id: Int):
            LiveData<EventWithPhotos> = getEventWithPhotos(id).getDistinct()

    @Query("SELECT category FROM events")
    abstract fun getcategories():LiveData<List<String>>

    @Transaction
    @Query("SELECT * FROM events WHERE category LIKE '%' || :category || '%'")
    protected abstract fun getDistinctEventByCategory(category: String) : LiveData<List<EventWithPhotos>>

    fun getEventByCategory(category: String):
            LiveData<List<EventWithPhotos>> = getDistinctEventByCategory(category).getDistinct()

    @Transaction
    @Query("SELECT * FROM events ORDER BY start_time ASC")
    abstract fun getEventsWithPhotos(): LiveData<List<EventWithPhotos>>

    fun insertPhotosForEvent(event: Event,photos:List<Photo>){
        for(photo in photos){
            photo.eventId = event.id
        }
        insertAllPhotos(photos)
    }


    @Transaction
    open fun updateAll(eventList: List<Event>,photos: List<Photo>) {
        deleteAll()
        deleteAllPhotos()
        insertAll(eventList)
        insertAllPhotos(photos)
    }

}