package com.wegoentertainment.igo.db

import androidx.room.TypeConverter
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class DateConverter {

    private val df:DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)

    @TypeConverter
    fun fromDateString(value:String) : Date? {
        try {
            return df.parse(value)
        }catch (e: ParseException) {
         e.printStackTrace()
        }
        return null
    }
}