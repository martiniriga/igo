package com.wegoentertainment.igo.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.wegoentertainment.igo.models.Bookmark
import com.wegoentertainment.igo.models.BookmarkPhoto
import com.wegoentertainment.igo.utils.getDistinct

@Dao
abstract class BookmarkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertEvent(bookmark: Bookmark)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(bookmarkList: List<Bookmark>)

    @Insert
    abstract fun insertAllPhotos(photos: List<BookmarkPhoto>)

    @Query("DELETE FROM bookmarks")
    abstract fun deleteAll()

    @Query("DELETE FROM bookmarkphotos")
    abstract fun deleteAllPhotos()

    @Query("DELETE FROM bookmarks WHERE id=:id")
    abstract fun deleteBookmark(id:Int)

    @Query("DELETE FROM bookmarkphotos WHERE eventId=:id")
    abstract fun deletePhotosByBookmarkId(id:Int)

    @Transaction
    @Query("SELECT * FROM bookmarks WHERE id=:eventId")
    protected abstract fun getBookmarkWithPhotosById(eventId: Int) : LiveData<BookmarkWithPhotos>

    fun getDistinctBookmarkWithPhotos(id: Int):
            LiveData<BookmarkWithPhotos> = getBookmarkWithPhotosById(id).getDistinct()

    @Transaction
    @Query("SELECT * FROM bookmarks ORDER BY start_time ASC")
    abstract fun getBookmarkWithPhotos(): LiveData<List<BookmarkWithPhotos>>

    fun insertPhotosForEvent(event: Bookmark,photos:List<BookmarkPhoto>){
        for(photo in photos){
            photo.eventId = event.id
        }
        insertAllPhotos(photos)
    }

    @Transaction
    open fun updateAll(bookmarkList: List<Bookmark>,photos: List<BookmarkPhoto>) {
        deleteAll()
        deleteAllPhotos()
        insertAll(bookmarkList)
        insertAllPhotos(photos)
    }

    @Transaction
    open fun deleteBookmarkById(id: Int) {
        deletePhotosByBookmarkId(id)
        deleteBookmark(id)
    }
}