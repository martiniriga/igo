package com.wegoentertainment.igo.db


import androidx.room.Embedded
import androidx.room.Relation
import com.wegoentertainment.igo.models.Bookmark
import com.wegoentertainment.igo.models.BookmarkPhoto
import com.wegoentertainment.igo.models.Photo


class BookmarkWithPhotos {
    @Embedded
    var event: Bookmark? = null

    @Relation(parentColumn = "id", entityColumn = "eventId",entity = Photo::class)
    var photos: List<BookmarkPhoto>? = null
}